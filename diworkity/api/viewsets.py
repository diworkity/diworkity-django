from django.shortcuts import render
from rest_framework import (mixins, viewsets, filters, status, pagination)
from rest_framework.decorators import action
from rest_framework.response import Response

import django_filters
from django_filters.rest_framework import DjangoFilterBackend, FilterSet

from api.models import Company, Office, Rating
from api.serializers import CompanyFullSerializer, RatingCreationSerializer, OfficeSerializer, OfficeCreationSerializer, RatingSerializer, CompanySearchSerializer

class CompanyFilters(django_filters.FilterSet):
    name = django_filters.CharFilter(field_name='name',
                                         lookup_expr='icontains')

    class Meta:
        model = Company
        fields = ('name',)

class CompanyViewSet(mixins.RetrieveModelMixin, mixins.UpdateModelMixin, mixins.ListModelMixin,
                    viewsets.GenericViewSet):
    queryset = Company.objects.all()
    serializer_class = CompanyFullSerializer
    filter_backends = [DjangoFilterBackend, filters.SearchFilter]
    filter_class = CompanyFilters
    serializer_classes = {
        'list': CompanySearchSerializer,
    }

    def get_serializer_class(self):
        return self.serializer_classes.get(self.action,
                                           super().get_serializer_class())

    @action(detail=True, methods=['GET'])
    def ratings(self, request, pk) :
        ratings = self.get_object().ratings()
        office_filter = request.GET.get('office', None)
        minority_filter = request.GET.get('minority', None)
        sort_order = request.GET.get('sort', None)
        if office_filter :
            ratings = ratings.filter(office__id=office_filter)
        if minority_filter :
            ratings = ratings.filter(minority__icontains=minority_filter)
        if sort_order:
            if sort_order == 'least_recent' :
                ratings = ratings.order_by('id')
            elif sort_order == 'best':
                ratings = ratings.order_by('-personal_rating')
            elif sort_order == 'worst':
                ratings = ratings.order_by('personal_rating')

        paginator = pagination.PageNumberPagination()
        page = paginator.paginate_queryset(ratings, request)
        serializer = RatingSerializer(page, many=True, context={'request': request})
        return Response(paginator.get_paginated_response(serializer.data).data, status=status.HTTP_200_OK)

class OfficeViewSet(mixins.CreateModelMixin, mixins.RetrieveModelMixin, mixins.UpdateModelMixin,
                    viewsets.GenericViewSet):
    queryset = Office.objects.all()
    serializer_class = OfficeSerializer
    serializer_classes = {
        'list': OfficeSerializer,
        'create': OfficeCreationSerializer
    }

    def get_serializer_class(self):
        return self.serializer_classes.get(self.action,
                                           super().get_serializer_class())

    def create(self, request, *args, **kwargs):
        return super(OfficeViewSet, self).create(request, *args, **kwargs)

class RatingViewSet(mixins.CreateModelMixin, viewsets.GenericViewSet):
    queryset = Rating.objects.all()
    serializer_class = RatingCreationSerializer

    def create(self, request, *args, **kwargs):
        officeData = request.data.get("office", None)
        if(isinstance(officeData, dict)) :
            company = Company.objects.get(id=officeData.get('company'))
            existingOfficeSet = Office.objects.filter(company=company, name=officeData.get('name'))
            if existingOfficeSet.exists() :
                office = existingOfficeSet.first()
            else :
                office = Office.objects.create(name=officeData.get('name'), company=company)
            request.data['office'] = office.id
        return super(RatingViewSet, self).create(request, *args, **kwargs)