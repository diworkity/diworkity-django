from django.db import models
from django.contrib.auth.models import User
from datetime import datetime
from os.path import splitext

from django.core.validators import MinValueValidator, MaxValueValidator

rating_validators = [MinValueValidator(0), MaxValueValidator(5)]

def images_handler(instance, filename):
	today = datetime.today()

	return '{year}-{week}-{name}{ext}'.format(
		year=today.year,
		week=today.isocalendar()[1],
		name=today.strftime('%m%d%H%M%S%f'),
		ext=splitext(filename)[1].lower())

class Company(models.Model):
    name = models.CharField(max_length=255, blank=False)
    logo = models.ImageField(upload_to=images_handler, blank=True)
    user = models.ForeignKey(User, blank=True, null=True, on_delete=models.SET_NULL)

    def __str__(self):
        return self.name

    def ratings(self):
        return Rating.objects.filter(office__in=self.offices.all()).order_by('-id')

class Office(models.Model):
    name = models.CharField(max_length=255, blank=False)
    company = models.ForeignKey(Company, on_delete=models.CASCADE, related_name="offices")

    def __str__(self):
        return self.company.name + " / " + self.name

class Rating(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    office = models.ForeignKey(Office, on_delete=models.CASCADE)

    role = models.CharField(max_length=255, blank=False)
    minority = models.CharField(max_length=255, blank=False)
    minority_description = models.CharField(max_length=255, blank=True)

    personal_rating = models.IntegerField(validators=rating_validators)
    progression_rating = models.IntegerField(validators=rating_validators, blank=True, null=True)
    payment_rating = models.IntegerField(validators=rating_validators, blank=True, null=True)
    culture_rating = models.IntegerField(validators=rating_validators, blank=True, null=True)
    seniority_representation_rating = models.IntegerField(validators=rating_validators, blank=True, null=True)
    flexibility_rating = models.IntegerField(validators=rating_validators, blank=True, null=True)
    attrition_rating = models.IntegerField(validators=rating_validators, blank=True, null=True)

    bi_line = models.CharField(max_length=255, blank=False)
    comments = models.TextField(default='', blank=True)

    def __str__(self):
        return self.user.first_name + " - " + str(self.office)

