# Generated by Django 3.2.7 on 2021-09-30 05:01

import api.models
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0007_auto_20210915_0358'),
    ]

    operations = [
        migrations.AddField(
            model_name='company',
            name='logo',
            field=models.ImageField(blank=True, upload_to=api.models.images_handler),
        ),
    ]
