from rest_framework import serializers, pagination
from django.db.models import Avg

from api.models import Company, Office, Rating

class OfficeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Office
        fields = '__all__'

class OfficeNameSerializer(serializers.ModelSerializer):
    class Meta:
        model = Office
        fields = ('id', 'name',)

class CompanySerializer(serializers.ModelSerializer):
    offices = OfficeSerializer(many=True, read_only=True)

    class Meta:
        model = Company
        fields = ('id', 'name', 'offices')

class RatingSerializer(serializers.ModelSerializer):
    office = OfficeNameSerializer(many=False, read_only=True)

    class Meta:
        model = Rating
        fields = ('office', 'role', 'minority', 'minority_description', 'personal_rating', 'progression_rating', 'payment_rating', 'culture_rating', 'seniority_representation_rating', 'flexibility_rating', 'attrition_rating', 'bi_line', 'comments',)

class RatingCreationSerializer(serializers.ModelSerializer):
    user = serializers.PrimaryKeyRelatedField(read_only=True, default=serializers.CurrentUserDefault())

    class Meta:
        model = Rating
        fields = '__all__'

    def save(self, **kwargs):
        kwargs["user"] = self.fields["user"].get_default()
        return super().save(**kwargs)

class CompanyFullSerializer(serializers.ModelSerializer):
    offices = OfficeNameSerializer(many=True, read_only=True)
    ratings = serializers.SerializerMethodField()
    avg_ratings = serializers.SerializerMethodField()
    is_creator = serializers.SerializerMethodField()

    def get_ratings(self, obj):
        paginator = pagination.PageNumberPagination()
        page = paginator.paginate_queryset(obj.ratings(), self.context['request'])
        serializer = RatingSerializer(page, many=True, context={'request': self.context['request']})
        return paginator.get_paginated_response(serializer.data).data

    @staticmethod
    def get_avg_ratings(obj):
        ratings = Rating.objects.filter(office__in=obj.offices.all())
        return ratings.aggregate(
            personal_rating=Avg('personal_rating'), 
            progression_rating=Avg('progression_rating'),
            payment_rating=Avg('payment_rating'),
            culture_rating=Avg('culture_rating'),
            seniority_representation_rating=Avg('seniority_representation_rating'),
            flexibility_rating=Avg('flexibility_rating'),
            attrition_rating=Avg('attrition_rating'),
            )

    def get_is_creator(self, obj):
        if obj.user is None :
            return False

        request = self.context.get('request')
        user = request.user
        return obj.user == user

    class Meta:
        model = Company
        fields = ('id', 'name', 'offices', 'ratings', 'avg_ratings', 'logo', 'is_creator')

class CompanySearchSerializer(serializers.ModelSerializer):
    office_count = serializers.SerializerMethodField()
    avg_rating = serializers.SerializerMethodField()
    rating_count = serializers.SerializerMethodField()

    class Meta:
        model = Company
        fields = ('id', 'name', 'office_count', 'rating_count', 'avg_rating', 'logo')

    @staticmethod
    def get_office_count(obj):
        return obj.offices.count()

    @staticmethod
    def get_rating_count(obj):
        return Rating.objects.filter(office__in=obj.offices.all()).count()

    @staticmethod
    def get_avg_rating(obj):
        ratings = Rating.objects.filter(office__in=obj.offices.all())
        return ratings.aggregate(
            personal_rating=Avg('personal_rating'), 
            )

class CompanyCreationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = ('name', 'id')

class OfficeCreationSerializer(serializers.ModelSerializer):
    company = CompanyCreationSerializer(many=False)

    class Meta:
        model = Office
        fields = ['id', 'name', 'company']

    def create(self, validated_data):
        company = validated_data.pop('company')
        companyObj = Company.objects.create(**company, user=self.context['request'].user)
        officeObj = Office.objects.create(company=companyObj, **validated_data)
        return officeObj