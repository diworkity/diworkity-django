from django.contrib import admin
from api.models import *

@admin.register(Company)
class CompanyAdmin(admin.ModelAdmin) :
	list_display = ('__str__',)

@admin.register(Office)
class OfficeAdmin(admin.ModelAdmin) :
	list_display = ('__str__',)

@admin.register(Rating)
class RatingAdmin(admin.ModelAdmin) :
	list_display = ('__str__',)